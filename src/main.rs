struct Registers {
    // Registers a-d: 8-bit unsigned ints
    a: u8,
    b: u8,
    c: u8,
    d: u8,

    // Registers e-h: stacks of 8-bit unsigned ints
    e: Vec<u8>,
    f: Vec<u8>,
    g: Vec<u8>,
    h: Vec<u8>,

    // Special Registers i-l
    i: usize, // current instruction pointer (note that this is a usize, not a u8)
    j: u8, // conditional flags from last arithmetic or comparison operation
    k: u8, // configuration flags, as defined by the user or an instruction
    l: u8, // error status

    // Read-only Registers m-p
    m: u8, // compiler/interpreter behaviour
    n: u8, // compiler/interpreter identification
    o: u8, // system information
    p: u8, // user-defined configuration flags
}

fn nop(state: &mut Registers) {}

fn invalid(state: &mut Registers) { // invalid instructions are non-breaking but generate an error flag
    state.l = state.l | 0b1000_0000;
    if state.k & 0b1000_0000 != 0 {
        eprintln!("[error] Invalid instruction at {}", state.i); // print error in debug mode
    }
}

fn get_func(instruction: &str) -> impl Fn(&mut Registers) {
    if instruction.chars().next().unwrap() == '#' {
        nop
    } else {
        match instruction.trim() {
            _ => invalid
        }
    }
}

fn main() {
    println!("Hello, world!");
}
